//
//  ViewController.swift
//  ToDoApp
//
//  Created by Sourav Dewett on 2019-03-28.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import UIKit

class ToDoListViewController: UITableViewController {

    var item = ["Program", "Go Home", "Sleep"]
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = self.defaults.array(forKey: "NewItem") as? [String] {
            self.item = item
        }
        tableView.separatorStyle = .none
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // MARK: - Table View DataSource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoItemCell", for: indexPath)
        cell.textLabel?.text = "\(item[indexPath.row])"
       
        return cell
        
    }
    
    // MARK:- TableView Delegate Methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("\(indexPath.row) == \(item[indexPath.row])")
        
        
        if(tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark) {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }else { 
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    // MARK: - Add New Item
    
    @IBAction func AddButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        let alert = UIAlertController(title: "Would You Like To Add New Task", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add Task", style: .default)
        
        { (action) in
            self.item.append(textField.text!)
            self.defaults.set(self.item, forKey: "NewItem")
            self.tableView.reloadData()
            print("Added Item")
            
        }
        
        alert.addTextField { (alerttextField) in
            textField = alerttextField
            textField.placeholder = "add new item"
            
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    

}

